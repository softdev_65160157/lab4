/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author informatics
 */
public class Table {
    private char[][] table ={{'7','8','9'},
                            {'4','5','6'},
                            {'1','2','3'}};    
    private Player currentPlayer;
    private Player player1;
    private Player player2;
     

  
    private char  num;
    private char [][] tableTest ={{'7','8','9'},
                                {'4','5','6'},
                                {'1','2','3'}};
    private boolean check;
    Table(Player player1,Player player2 ){
        this.player1=player1;
        this.player2=player2;
        this.currentPlayer=player1;
    }
  
    
    public char[][] getTableTest() {
        return tableTest;
    }
    
    public char[][] getTable(){
        return table;
    }
    
   public boolean setNum (char num){
        this.num=num;
          for(int i = 0; i < 3;i++ ){
            for(int j = 0; j < 3;j++){
               if((table[i][j])==tableTest[i][j]){
                   check=true;
                     if((table[i][j])==num){
                          table[i][j]=currentPlayer.getSymbal();
                         
                    return true;
   }
              }
               check=false;
           }
         }
          return false;
   }

    public boolean getCheck() {
        return check;
    }
   


 
    public void changePlayer() {
      
            if(currentPlayer==player1){
                currentPlayer=player2;
            }else{
                 currentPlayer=player1;           
            }
    }
    
 
    Player getCurrentPlayer() { 
        return currentPlayer;
    }
    

  
    public boolean checkRow(){
          for(int i = 0; i < 3;i++){
                if((table[i][0])==currentPlayer.getSymbal()&&
                        (table[i][1])==currentPlayer.getSymbal()&&
                        (table[i][2])==currentPlayer.getSymbal()){
                    return true;
                    }     
            } 
           return false;

    }
     public boolean checkCol(){
          for(int i = 0; i < 3;i++){
                if((table[0][i])==currentPlayer.getSymbal()&&
                        (table[1][i])==currentPlayer.getSymbal()&&
                        (table[2][i])==currentPlayer.getSymbal()){
                    return true;
                    }     
            } 
           return false;

    }
    public boolean checkX1(){
            for(int i = 0; i < 3;i++){
                if((table[i][i])!=currentPlayer.getSymbal()){
                    return false;
                }     
            }
            return true;
    }

    public boolean checkX2(){
    for(int i = 0; i < 3;i++){
                if((table[i][2-i])!=currentPlayer.getSymbal() ){
                    return false;
                    }     
            } 
            return true;
    }
    public boolean isWin(){
        if(checkRow()){
           return true;
       }if(checkCol()){
           return true;
       }if(checkX1()){
           return true; 
       }if(checkX2()){
           return true;
       }
       return false;
    }
    
    public boolean isDraw(){
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] != 'X' && table[i][j] != 'O') {
                    return false;
                }
            }
        }
        return true;
      }
    
  
    
    
   
    
}
 