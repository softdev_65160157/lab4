/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;
/**
 *
 * @author informatics
 */
public class Player {
    private char symbal;
    private int winCount,loseCount,drawCount;
    private char player1,player2 ,currentPlayer;;
    
    public Player(char symbal){
        this.symbal=symbal;
    }
    
    public char getSymbal() {
        return symbal;
    }

    public int getWinCount() {
        return winCount;
    }

    public int getLoseCount() {
        return loseCount;
    }

    public int getDrawCount() {
        return drawCount;
    }
     public void win(){
        winCount++;
    }
      public void lose(){
        loseCount++;
    }
       public void draw(){
        drawCount++;
    } 
      
}
